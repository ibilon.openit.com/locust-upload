from string import ascii_uppercase, digits
from locust import HttpUser, task
from multidict import MultiDict
from random import choice
from pathlib import Path

payloadfile = Path('payload.txt')
payload     = payloadfile.read_text()

class Client(HttpUser):
    @task
    def upload_licpoll(self):
        files = MultiDict([ ('file[]', random_payload()) for i in range(10) ])
        self.client.post('/api/data?type=archiver', files=files, verify=False)

def random_string():
    pool = ascii_uppercase + digits
    sequence = [ choice(pool) for i in range(64) ]
    return ''.join(sequence)
    
def random_signature():
    sequence = random_string()
    return f'@SIGNATURE: C2(S2):{sequence}\r\n'

def random_payload():
    return payload + random_signature()
