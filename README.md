# Locust Upload

Load testing for uploading files to the Core Server.

## Install

```sh
git clone git@gitlab.com:ibilon.openit.com/locust-upload.git
cd locust-upload
python -m pip install -r requirements.txt
```

## Run

Open terminal and `cd` to `locust-upload` repository.

Run a master node.

```sh
locust --master
```

Run a worker node.

```sh
locust --worker --master-host <master hostname>
```
